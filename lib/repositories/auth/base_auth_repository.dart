
import 'package:firebase_bloc_notes/models/models.dart';
import 'package:firebase_bloc_notes/repositories/base_repository.dart';

abstract class BaseAuthRepository extends BaseRepository {
  Future<User> loginAnonymously();
  Future<User> signUpWithEmailAndPassword({String email, String password});
  Future<User> loginWithEmailAndPassword({String email, String password});
  Future<User> logout();
  Future<User> getCurrentUser();
  bool isAnonymous();
}
